using System.Linq;
using UnityEngine.SceneManagement;

namespace JonathanDefraiteur.TreasureHunt
{
    public static class InterfaceHelper
    {
        public static T[] FindObjectsOfType<T>()
        {
            return FindObjectsOfType<T>(false);
        }
        
        public static T[] FindObjectsOfType<T>(bool includeInactive)
        {
            return SceneManager
                .GetActiveScene()
                .GetRootGameObjects()
                .SelectMany(go => go.GetComponentsInChildren<T>(includeInactive))
                .ToArray();
        }
    }
}