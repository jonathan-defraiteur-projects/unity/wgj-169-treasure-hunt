using JonathanDefraiteur.TreasureHunt.Data;
using UnityEngine;

namespace JonathanDefraiteur.TreasureHunt
{
    [System.Serializable]
    public class WorldTreasure
    {
        [SerializeField] private Treasure treasure;
        [SerializeField] private Vector3 position;

        public Treasure Treasure => treasure;
        public Vector3 Position => position;
    }
}