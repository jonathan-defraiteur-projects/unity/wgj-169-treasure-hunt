namespace JonathanDefraiteur.TreasureHunt
{
    public interface ITreasureSource
    {
        WorldTreasure[] GetTreasureAndLocations();
    }
}