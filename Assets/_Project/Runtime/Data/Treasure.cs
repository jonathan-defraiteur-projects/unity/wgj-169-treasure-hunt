using System;
using UnityEngine;

namespace JonathanDefraiteur.TreasureHunt.Data
{
    [CreateAssetMenu(fileName = "New Treasure", menuName = "Treasure Hunt/Treasure", order = 0)]
    public class Treasure : ScriptableObject
    {
        [SerializeField] private string humanName = "";
        [SerializeField] private string description = "";
        [SerializeField] private Sprite icon = null;
        
        public string Name => name;
        public string HumanName => humanName;
        public string Description => description;
        public Sprite Icon => icon;
        
        private void OnEnable()
        {
            SetDefaultRequiredData();
        }

        private void OnValidate()
        {
            SetDefaultRequiredData();
        }

        private void SetDefaultRequiredData()
        {
            if (string.IsNullOrEmpty(humanName)) {
                humanName = name;
            }
        }
    }
}