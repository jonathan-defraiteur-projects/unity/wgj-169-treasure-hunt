﻿using ECM.Controllers;
using JonathanDefraiteur.TreasureHunt.Input;
using UnityEngine;
using UnityEngine.InputSystem;

namespace JonathanDefraiteur.TreasureHunt
{
    public class CharacterController : BaseCharacterController, InputMaster.IPlayerActions
    {
        private InputMaster _inputMaster = null;

        private void OnEnable()
        {
            if (_inputMaster == null) {
                _inputMaster = new InputMaster();
                _inputMaster.Player.SetCallbacks(this);
            }
            _inputMaster.Player.Enable();
        }

        private void OnDisable()
        {
            _inputMaster.Player.Disable();
        }
        
        protected override void HandleInput()
        {
            // Handle by IPlayerActions implementations
        }

        public void OnMove(InputAction.CallbackContext _context)
        {
            var inputValue = _context.ReadValue<Vector2>();
            moveDirection = new Vector3 {
                x = inputValue.x,
                y = 0.0f,
                z = inputValue.y
            };
        }

        public void OnLook(InputAction.CallbackContext _context)
        {
            // throw new System.NotImplementedException();
        }

        public void OnFire(InputAction.CallbackContext _context)
        {
            // throw new System.NotImplementedException();
        }
    }
}
