using System.Collections.Generic;
using UnityEngine;

namespace JonathanDefraiteur.TreasureHunt
{
    public class TreasureSourceBehaviour : MonoBehaviour, ITreasureSource
    {
        [SerializeField] private List<WorldTreasure> worldTreasures = new List<WorldTreasure>();

        private void OnDrawGizmosSelected()
        {
            foreach (WorldTreasure worldTreasure in worldTreasures) {
                Gizmos.DrawSphere(worldTreasure.Position, 1f);
            }
        }

        public WorldTreasure[] GetTreasureAndLocations()
        {
            return worldTreasures.ToArray();
        }
    }
}