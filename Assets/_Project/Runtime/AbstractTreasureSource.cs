namespace JonathanDefraiteur.TreasureHunt
{
    abstract class AbstractTreasureSource : ITreasureSource
    {
        public abstract WorldTreasure[] GetTreasureAndLocations();

        public static ITreasureSource[] GetSources()
        {
            return InterfaceHelper.FindObjectsOfType<ITreasureSource>();
        }
    }
}