using System;
using UnityEngine;
using UnityEngine.UI;

namespace JonathanDefraiteur.TreasureHunt
{
    public class TreasureCountUI : MonoBehaviour
    {
        [SerializeField] private Text textUi = null;

        private void Start()
        {
            if (!textUi && TryGetComponent(out textUi)) {
                Debug.LogWarning($"No {nameof(Text)} component on {name}.");
                return;
            }

            ITreasureSource[] sources = AbstractTreasureSource.GetSources();
            int count = 0;
            foreach (ITreasureSource source in sources) {
                count += source.GetTreasureAndLocations().Length;
            }

            textUi.text = count.ToString();
        }
    }
}